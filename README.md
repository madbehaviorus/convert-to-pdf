


# convert-to-pdf...

is a script who use [wkhtmltopdf](https://wkhtmltopdf.org) to convert not only one file, but all files in one directory

## Help:

1. The first command is for the exacty path to the directory, in there are the *.html files.
2. Second command is for the name of the outputfiles."
3. If you want, insert the the maximum (\$max > 0) of this html files, do you want to convert.
   Default = all files in the directory.

like:
convert-to-pdf \$path \$outputfilename

or:
convert-to-pdf \$path \$outputfilename \$max

