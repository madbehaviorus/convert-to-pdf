#/bin/bash
#
# madbehaviorus 
# 
# 2021-10-26
# v0.5
# 
# this script is for convertion from local html files to pdf

# variables
path=$1
out=$2;
ein=$3;

i=1;
temp=$(echo "$path" | tail -c 2)


# help
if [ $path == "-h" ]
then
        echo ""
        echo ""
        echo "Help:"
        echo ""
        echo "1. The first command is for the exacty path to the directory, in there are the *.html files. "
        echo "2. Second command is for the name of the outputfiles."
        echo "3. If you want, insert the the maximum (\$max > 0) of this html files, do you want to convert. "
        echo "   Default = all files in the directory."
        echo ""
        echo "like:"
        echo "convert-to-pdf \$path \$outputfilename"
        echo ""
        echo "or:"
        echo "convert-to-pdf \$path \$outputfilename \$max"
        echo ""
        echo "" 
        exit
fi

# test installed wkhtmltopdf
if [ ! -f /usr/bin/wkhtmltopdf ] 
then
        echo "Please install wkhtmltopdf." 
        exit
fi

# print 
echo $ein;
echo $temp;

# test path
if [ $temp == "\\" ]
then
        ls -a $path*.html > books.txt
else
        if [ -d $path ]
        then
                ls -a $path/*.html > books.txt
        else
                echo "Path does not exist!";
                exit;
        fi
fi



# looking for the inserted sites
if [[ $ein =~ ^-?[0-9]+$ ]] && (( $ein > 1 ))
then
        echo "ok"
else
        ein=$(cat books.txt | wc -l)
fi



# the convert commands
while (( $i <= $ein ))
do
        page=$(cat books.txt | head -n $i | tail -n 1 );
        echo "Convert: "  $path$page;
        wkhtmltopdf --no-background --enable-local-file-access  "$page" $out_$i.pdf
        echo "Success pdf Nr.: $i";
       ((i++));
done


# rm list
rm books.txt

echo ""
echo ""
echo "done!";
echo ""
echo ""



